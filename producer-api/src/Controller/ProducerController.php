<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\JsonResponse;

class ProducerController extends AbstractController
{
    /**
     * @Route("/publish", name="publish_index")
     */
    public function publish(): JsonResponse
    {
        $data = [
            'message' => 'It\'s all good m8!'
        ];

        return new JsonResponse($data);
    }
}